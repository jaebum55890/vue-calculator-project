import Vue from 'vue'
import VueRouter from 'vue-router'
import calculator from '../views/calculator.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'calculator',
    component: calculator
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
